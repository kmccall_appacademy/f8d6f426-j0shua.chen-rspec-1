def add(num_one, num_two)
  return num_one + num_two
end

def subtract(num_one, num_two)
  return num_one - num_two
end

def sum(array_of_num)
  return 0 if array_of_num.empty?
  return array_of_num.reduce(:+)
end

def multiply(num_one, num_two)
  return num_one * num_two
end

def power(num_one, num_two)
  return num_one**num_two
end

def factorial(num)
  return 1 if num == 0
  return (1..num).reduce(:*)
end
