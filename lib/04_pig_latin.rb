def translate(string)
  new_string = []
  string.split(" ").each do |x|
    new_string << helper(x)
  end
  return new_string.join(" ")

end

def helper(word)
  vowels = "aeiou"
  until vowels.include?(word[0]) && (word[0].downcase != "u" && word[-1].downcase != "q")
    word = word[1..-1] + word[0]
  end
  return word + "ay"
end
