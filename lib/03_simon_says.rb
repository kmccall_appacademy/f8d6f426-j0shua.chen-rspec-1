def echo(string)
  return string
end

def shout(string)
  return string.upcase
end

def repeat(string, num_of_times = 2)
  new_string=[]
  num_of_times.times {new_string << string}
  return new_string.join(" ")
end

def start_of_word(string, num_of_letters)
  return string[0..(num_of_letters - 1)]
end

def first_word(string)
  return string.split(" ").first
end

def titleize(string)
  title = []
  little_words = ["or", "the", "and", "over"]
  string.split(" ").each_with_index do |x, i|
    if little_words.include?(x) && i > 0
      title << x
    else
      title << x.capitalize
    end
  end
  return title.join(" ")
end
